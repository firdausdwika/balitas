/*=================================================
do not edit this file
=================================================*/
var $ = jQuery.noConflict();

(function($) {
  'use strict';

/*=================================================
variable
=================================================*/
var $html = $('html');
var $body = $('body');

/*=================================================
ie10 viewport fix
=================================================*/
  (function() {
    'use strict';
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      var msViewportStyle = document.createElement('style')
      msViewportStyle.appendChild(
        document.createTextNode(
          '@-ms-viewport{width:auto!important}'
        )
      )
      document.querySelector('head').appendChild(msViewportStyle)
    }
  })();

/*=================================================
platform detect
=================================================*/
  if ($html.hasClass('desktop')) {
    $html.addClass('non-mobile');
    var isMobile = false;
  } else {
    $html.addClass('is-mobile');
    var isMobile = true;
  }
  if ($html.hasClass('ie9')) {
    var isIE9 = true;
  }

/*=================================================
preloader
=================================================*/
  function fn_loader() {
    $('#loader').velocity('fadeOut', {
      delay: 500,
      duration: 1200,
      complete: function() {
        $('section').filter('.active').find('.animation').velocity('stop', true).velocity('transition.slideDownIn', {
          stagger: 120,
          easing: 'easeInOutCirc',
          queue: false
        });
      }
    });
    var sectionIndex = $('section').length;
    if (sectionIndex == 1) {
      $body.addClass('single');
    }
  }

/*=================================================
core
=================================================*/
  function fn_core() {
    $body.addClass(_color); // add color class
    if (_gradient) {
      $body.addClass('gradient' + _gradientStyle + ' gradient'); // gradient
    }

    // bind click event to all internal page anchors
    $('a[href=#]').bind('click', function(e) {
      e.preventDefault();
    });

    // progress bar width
    $('.progress-bar').each(function() {
      var $this = $(this);

      $this.css('width', $this.attr('aria-valuenow') + '%');
    });

    // services image
    $('.services-grid-img').each(function() {
      var $this = $(this);

      $this.css('background-image', 'url(' + $this.attr('data-img') + ')');
    });
  }

/*=================================================
menu
=================================================*/
  function fn_menu() {
    var $menuToggle = $('.menu-toggle');
    var $menu = $('.menu');
    var $menuLi = $('.menu-ul').find('li');
    var $subscribeBtn = $('a[data-to="subscribe"]');

    $('a[data-section]').on('click', function(e) {
      var $this = $(this);
      var $activeMenu = $menuLi.find('a.active');
      var dataSection = $this.attr('data-section');
      var $activeSection = $('section.active');
      var activeSectionId = $activeSection.attr('id');
      var sectionIndex = $(dataSection).index()-1;

      e.preventDefault();

      if (!$this.hasClass('active')) {
        $body.removeClass('menu-in');
        fn_menuOut();
        fn_fadeOutAnimation();
      }

      function fn_fadeOutAnimation() {
        $menuToggle.addClass('unclickable');
        $activeSection.find('.animation').velocity('stop', true).velocity('transition.slideUpOut', {
          display: null,
          stagger: 120,
          easing: 'easeInOutCirc',
          queue: false,
          complete: function() {
            $('#loader').velocity('fadeIn', {
              duration: 400,
              complete: function() {
                if ($this.parents($menuLi)) {
                  $this.addClass('active');
                }
                $activeMenu.add($activeSection).removeClass('active');
                $(dataSection).addClass('active');
                $body.removeClass('intro-in about-in services-in contact-in');
                $body.addClass(dataSection.replace('#', '') + '-in');
                if (_bg == 1 && _bgImgStyle == 2) {
                  $('#img').vegas('jump', sectionIndex);
                }
              }
            }).velocity('fadeOut', {
              duration: 1200,
              complete: fn_fadeInAnimation
            });
          }
        });
      }

      function fn_fadeInAnimation() {
        $(dataSection).find('.animation').velocity('stop', true).velocity('transition.slideDownIn', {
          stagger: 120,
          easing: 'easeInOutCirc',
          queue: false,
          complete: function() {
            $menuToggle.removeClass('unclickable');
          }
        });
      }
    });

    $(document).on('click', '.menu-toggle:not(.unclickable)', function(e) {
      e.preventDefault();

      if ($body.hasClass('subscribe-in')) {
        fn_subscribeOut();
      } else {
        $body.toggleClass('menu-in');
        if ($body.hasClass('menu-in')) {
          fn_menuIn();
        } else {
          fn_menuOut();
        }
      }
    });

    $('a[data-to="subscribe"]').on('click', function(e) {
      e.preventDefault();
      fn_subscribeIn();
    });

    function fn_menuIn() {
      $menu.velocity('stop', true).velocity('transition.expandIn', {
        duration: 500,
        easing: 'easeInOutCirc',
        queue: false
      });
      $menuLi.velocity('stop', true).velocity('transition.slideUpIn', {
        stagger: 120,
        easing: 'easeInOutCirc',
        queue: false
      });
    }

    function fn_menuOut() {
      $menuLi.velocity('transition.slideUpOut', {
        display: null,
        stagger: 80,
        easing: 'easeInOutCirc',
        queue: false
      });
      $menu.velocity('stop', true).velocity('transition.expandOut', {
        duration: 500,
        easing: 'easeInOutCirc',
        queue: false
      });
    }

    function fn_subscribeOut() {
      $body.removeClass('subscribe-in');
      $('#subscribe-form').velocity('stop', true).velocity('transition.slideUpOut', {
        display: null,
        duration: 500,
        easing: 'easeInOutCirc',
        queue: false
      });
      $('#subscribe').velocity('stop', true).velocity('transition.expandOut', {
        duration: 500,
        easing: 'easeInOutCirc',
        queue: false
      });
    }

    function fn_subscribeIn() {
      $body.addClass('subscribe-in');
      if ($body.hasClass('subscribe-in')) {
        $('#subscribe').velocity('stop', true).velocity('transition.expandIn', {
          duration: 500,
          easing: 'easeInOutCirc',
          queue: false
        });
        $('#subscribe-form').velocity('stop', true).velocity('transition.slideUpIn', {
          duration: 500,
          easing: 'easeInOutCirc',
          queue: false
        });
      }
    }
  }

/*=================================================
background
=================================================*/
  function fn_bg() {
    if (_bg == 1) {
      fn_imgBg();
    } else if (_bg == 2) {
      fn_videoBg();
    } else if (_bg == 3) {
      fn_ytVideoBg();
    }
  }

/*=================================================
image background
=================================================*/
  function fn_imgBg() {
    $('#video').remove();
    if (_bgImgStyle == 2) {
      var _bgSliderautoplay = false;
    } else {
      var _bgSliderautoplay = true;
    }
    
    var _imgAmount = $('section').length;
    var _bgSliderSlides = [];
    for (var i = 1; i <= _imgAmount; i++) {
      _bgSliderSlides.push({
        src: 'assets/img/bg/' + (i < 10 ? '0' + i : i) + '.jpg'
      })
    }

    $body.addClass('img-bg');

    $('#img').vegas({
      preload: true,
      timer: false,
      delay: _bgSliderDelay,
      autoplay: _bgSliderautoplay,
      transition: 'fade',
      overlay: false,
      animation: false,
      slides: _bgSliderSlides
    });

    if (_bgImgStyle == 1) {
      $('#img').vegas('options', 'slides', [{src: 'assets/img/bg/01.jpg'}]);
    }
  }

/*=================================================
youtube video background
=================================================*/
  function fn_ytVideoBg() {
    var $video = $('#video');
    var $volume = $('#volume');
    var $img = $('#img');

    $body.addClass('yt-video-bg');
    if (!isMobile) {
      $video.attr('data-property', '{videoURL: _ytUrl, autoPlay: true, loop: _ytLoop, startAt: _ytStart, stopAt: _ytEnd, mute: _ytMute, quality: _ytQuality, realfullscreen: true, optimizeDisplay: true, addRaster: false, showYTLogo: false, showControls: false, stopMovieOnBlur: false, containment: "self"}');
      $video.YTPlayer();

      if (_ytRemoveVolumeIcon) {
        $volume.remove();
      }
      (_ytMute) ? $volume.addClass('ion-android-volume-off') : $volume.addClass('ion-android-volume-up');      
      $volume.on('click', function() {
        $volume.toggleClass('ion-android-volume-off ion-android-volume-up', function() {
          ($volume.hasClass('ion-android-volume-up')) ? $video.muteYTPVolume() : $video.unmuteYTPVolume();
        }());
      });
    }
  }

/*=================================================
video background
=================================================*/
  function fn_videoBg() {
    var $video = $('#video');
    var $volume = $('#volume');

    $body.addClass('video-bg');
    if (!isMobile) {
      $video.append('<video id="video-bg" autoplay loop><source src="assets/video/video.mp4" type="video/mp4"></video>');
      if (_videoMute) {
        var video = document.getElementById('video-bg');
        video.muted = true;
      }
      if (_removeVolumeIcon) {
        $volume.remove();
      }
      (_videoMute) ? $volume.addClass('ion-android-volume-off') : $volume.addClass('ion-android-volume-up');      
      $volume.on('click', function() {
        var video = document.getElementById('video-bg');
        $volume.toggleClass('ion-android-volume-off ion-android-volume-up', function() {
          ($volume.hasClass('ion-android-volume-up')) ? video.muted = true : video.muted = false;
        }());
      });
    }
  }

/*=================================================
countdown
=================================================*/
  function fn_countdown() {
    var $countdown = $('#countdown');

    if ($countdown.length) {
      if (_countdown) {
        $countdown.countdown({
          until: _countdownDate,
          timezone: _countdownTimezone,
          format: 'DHMS',
          significant: 1
        });

        fn_countdownToggle();
      } else {
        $('#countdown-toggle').add($countdown).remove();
      }
    }
  }

  function fn_bigCountdown() {
    $('.countdown-amount').viewportScale({
      'font-size': '40vw'
    });
    $('.countdown-period').viewportScale({
      'font-size': '10vw'
    });
  }

  function fn_countdownToggle() {
    $('.bfc-toggle-btn').on('click', function() {
      if(!$('#footer-toggle').is(':checked')) {
        $body.addClass('countdown-in');
      } else {
        $body.removeClass('countdown-in');
      }
    });
  }

/*=================================================
slider
=================================================*/
  function fn_slider() {
    var $servicesSlider = $('#services-slider');
    var $contactSlider = $('#contact-slider');
    var $bfcSlider = $('.bfc-slider');

    $bfcSlider.each(function() {
      var $this = $(this);

      $this.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true,
        dots: true,
        fade: true,
        cssEase: 'linear'
      });
      $('.slick-prev, .slick-next').text('');

      $this.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        // hold
      });

      $this.on('afterChange', function(event, slick, currentSlide){
        //$('.slick-current').find('.services-grid-content').fadeIn();
        if (!isMobile) {
          $this.parent('.scroll-block').perfectScrollbar('update');
        }
      });
    })
  }

/*=================================================
email validation
=================================================*/
  function fn_formValidation(email_address) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(email_address);
  }

/*=================================================
subscribe form
=================================================*/
  function fn_subscribe() {
    if (_subscribe == 1 || _subscribe == 2) {
      fn_subscribeForm();
    } else if (_subscribe == 3) {
      fn_mailchimp();
    }
  }

  /* mailchimp */
  function fn_mailchimp() {

    var $form = $('#subscribe-form');
    var $subscribeEmail = $('#subscribeFormEmail');

    $form.ajaxChimp({
      callback: fn_mailchimpStatus,
      language: 'eng',
      type: 'POST',
      url: _mailchimpUrl
    });

    function fn_mailchimpStatus (resp) {

      if (resp.result === 'error') {
        $subscribeEmail.focus();
        $('.subscribe-notice').addClass('visible');
      }
      else if (resp.result === 'success') {
        $form[0].reset();
        $subscribeEmail.blur();
        $('.subscribe-notice').addClass('visible');
      }
    }
  }

  /* php */
  function fn_subscribeForm() {

    var $form = $('#subscribe-form');
    var $subscribeEmail = $('#subscribeFormEmail');
    if (_subscribe == 1) {
      var url = 'assets/php/subscribe.php';
    } else if (_subscribe == 2) {
      var url = 'assets/php/local-subscribe.php';
    }

    $subscribeEmail.prop('type', 'text');

    $form.on('submit', function(e) {

      var subscribeEmailVal = $subscribeEmail.val();
      var $subscribeNotice = $('.subscribe-notice');
      var $submitButton = $form.find('button[type="submit"]');

      e.preventDefault();

      $submitButton.prop('disabled', true);

      if (!fn_formValidation(subscribeEmailVal)) {
        $subscribeNotice.stop(true).hide().addClass('visible').html(_subscribeError).fadeIn();
        $submitButton.prop('disabled', false);
        $subscribeEmail.focus();
      }
      else {
        $.ajax({
          type: 'POST',
          url: url,
          data: {
            email: subscribeEmailVal,
            emailAddress: _subscribeEmail
          },
          success: function() {
            $subscribeNotice.stop(true).hide().addClass('visible').html(_subscribeSuccess).fadeIn();

            $submitButton.prop('disabled', false);
            $form[0].reset();
            $subscribeEmail.blur();

          }
        });
      }
      return false;

    });

  }

/*=================================================
contact form
=================================================*/
  function fn_contactForm() {

    var $form = $('#contact-form');

    $form.on('submit', function(e) {

      var $input = $form.find('input, textarea');
      var contactNameVal = $('#contactFormName').val();
      var contactEmailVal = $('#contactFormEmail').val();
      var contactMessageVal = $('#contactFormMessage').val();
      var $contactNotice = $('.contact-notice');
      var $submitButton = $form.find('button[type="submit"]');

      e.preventDefault();

      if (contactNameVal == '' || contactEmailVal == '' || contactMessageVal == '') {
        $contactNotice.stop(true).hide().html(_contactInputError).fadeIn(500);
        $input.each(function() {
          if (this.value === '') {
            this.focus();
            return false;
          }
        });

      }

      else if (!fn_formValidation(contactEmailVal)) {
        $contactNotice.stop(true).hide().html(_contactEmailError).fadeIn(500);
        $('#contact-email').focus();
      }
      else {
        $.ajax({
          type: 'POST',
          url: 'assets/php/contact.php',
          data: {
            name: contactNameVal,
            email: contactEmailVal,
            message: contactMessageVal,
            emailAddress: _contactEmail
          },
          success: function() {
            $contactNotice.stop(true).hide().html(_contactSuccess).fadeIn(500);
            $form[0].reset();
            $input.blur();
          }
        });
      }
      return false;

    });
  }

/*=================================================
scrollbar
=================================================*/
  function fn_scrollbar() {
    var $scrollBlock = $('.scroll-block');

    // normal scroll on mobile
    if (isMobile) {
      $scrollBlock.addClass('mobile-scroll');
    } else {
      $scrollBlock.perfectScrollbar({
        suppressScrollX: true
      });
    }
  }

/*=================================================
disable section
=================================================*/
  function fn_disableSection() {
    for (var a in _disableSection) {
      if (_disableSection[a]) {
        var id = '#' + a;
        $(id).remove(); // remove section
        $('.menu-ul').find('a[data-section="' + id + '"]').parent().remove(); // remove menu
      }
    }
  }

/*=================================================
 * ie9 placeholder
=================================================*/
  function fn_placeholder() {
    if (isIE9) {
      $('input, textarea').placeholder({customClass: 'placeholder'});
    }
  }

/*=================================================
window on load
=================================================*/
  $(window).on('load', function() {

    fn_loader();
    fn_bigCountdown();
    fn_slider();
    fn_placeholder();

  });

/*=================================================
document on ready
=================================================*/
  $(document).on('ready', function() {

    fn_core();
    fn_scrollbar();
    fn_menu();
    fn_countdown();
    fn_bg();
    fn_subscribe();
    fn_contactForm();
    fn_disableSection();

  });

/*=================================================
window on resize
=================================================*/
  $(window).on('resize', function() {

  });

})(jQuery);