/***** YOU MAY REMOVE THIS readme.txt FILE AFTER READ *****/

01.jpg - intro section
02.jpg - about section
03.jpg - services section
04.jpg - contact section

if remove section from variable.js, e.g. remove services section
03.jpg will be contact section background image

services-grid-*.jpg - services grid image

yt-video-desktop.jpg - youtube video background
yt-video-mobile.jpg - youtube video background

video-desktop.jpg - video background
video-mobile.jpg - video background