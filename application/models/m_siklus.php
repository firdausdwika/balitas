<?php
class M_Siklus extends CI_Model{
    private $table="users";
    private $primary="id";
	
    function cek_user($username,$password){
        $this->db->where("username",$username);
        $this->db->where("password",$password);
        return $this->db->get("users");
    }
    function insertData($namatabel,$data){
   		$current = $this->db->insert($namatabel,$data);
   		return $current;
    }
    public function record()
    {
        return $this->db->count_all("siklus_hidup");
    }

    public function fetch_data($limit, $id) {
        $this->db->limit($limit);
        $this->db->where('id_siklus', $id);
        $query = $this->db->get("siklus_hidup");
        if ($query->num_rows() > 0) {
        foreach ($query->result() as $row) {
        $data[] = $row;
        }
        return $data;
        }
        return false;
    }

    public function getData()
    {
        $data = $this->db->query('select * from siklus_hidup');
        return $data->result_array();
    }
    public function editData($where="")
    {
        $data = $this->db->query('select * from siklus_hidup'.$where);
        return $data->result_array();
    }

    public function updateData($namatabel,$data,$where)
    {
        $data = $this->db->update($namatabel,$data,$where);
        return $data;
    }

    public function deleteData($namatabel,$where)
    {
        $data = $this->db->delete($namatabel,$where);
        return $data;
    }   
}