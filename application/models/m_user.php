<?php
class M_User extends CI_Model{
    private $table="users";
    private $primary="id";
	
    function cek_user($username,$password){
        $this->db->where("username",$username);
        $this->db->where("password",$password);
        return $this->db->get("users");
    }
    function insertData($namatabel,$data){
   		$current = $this->db->insert($namatabel,$data);
   		return $current;
    }
    public function getData()
    {
        $data = $this->db->query('select * from users');
        return $data->result_array();
    }
    public function editData($where="")
    {
        $data = $this->db->query('select * from users'.$where);
        return $data->result_array();
    }

    public function updateData($namatabel,$data,$where)
    {
        $data = $this->db->update($namatabel,$data,$where);
        return $data;
    }

    public function deleteData($namatabel,$where)
    {
        $data = $this->db->delete($namatabel,$where);
        return $data;
    }   
}