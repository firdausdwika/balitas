<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
    {
        $this->load->view('index');
    }
    public function jenis_hama(){
        $data = $this->m_hama->getData();
        $this->load->view('User/jenis_hama',array('data' => $data));
    }
    public function siklus_hama(){
        $data = $this->m_siklus->getData();
        $this->load->view('User/siklus',array('data' => $data));
    }
    public function identifikasi_hama(){
        $data = $this->m_identifikasi->getData();
        $this->load->view('User/identifikasi',array('data' => $data));
    }
    public function pengendalian_hama(){
        $data = $this->m_pengendalian->getData();
        $this->load->view('User/pengendalian',array('data' => $data));
    }

    public function details_pengendalian($id)
    {
        $res = $this->m_pengendalian->editData(" where id_pengendalian = '$id'");
        $data = array(
            "id" => $res[0]['id_pengendalian'],
            "gambar" => $res[0]['gambar'],
            "deskripsi" => $res[0]['deskripsi']
            );
        $this->load->view('pengendalian_hama',$data);
    }

    public function details_identifikasi($id)
    {
        $res = $this->m_identifikasi->editData(" where id_identifikasi = '$id'");
        $data = array(
            "id" => $res[0]['id_identifikasi'],
            "gejala_serangan" => $res[0]['gejala_serangan'],
            "perilaku" => $res[0]['perilaku'],
            "deskripsi" => $res[0]['deskripsi']
            );
        $this->load->view('identifikasi_hama',$data);
    }

    public function details_hama($id)
    {
        $res = $this->m_hama->editData(" where id_hama = '$id'");
        $data = array(
            "id" => $res[0]['id_hama'],
            "nama_hama" => $res[0]['nama_hama'],
            "nama_latin" => $res[0]['nama_latin'],
            "foto" => $res[0]['foto'],
            "deskripsi" => $res[0]['deskripsi']
            );
        $this->load->view('hama',$data);
    }

    public function details_siklus($id)
    {
        $res = $this->m_siklus->editData(" where id_siklus = '$id'");
        $data = array(
            "id" => $res[0]['id_siklus'],
            "siklus_hidup" => $res[0]['siklus_hidup'],
            "keterangan" => $res[0]['keterangan']
            );
        $this->load->view('siklus_hama',$data);
    }

    function __construct(){
        parent::__construct();
        $this->load->model('m_user');
        if($this->session->userdata('username')){
        	redirect('welcome');
        }
    }

	function login()
	{
		$this->load->view('welcome');
	}

    function register()
    {
        $this->load->view('register');
    }

    function daftar(){
        $username = $_POST['username'];
        $password = $_POST['password'];
        $nickname = $_POST['nickname'];
        $data = array(
            'username' => $username,
            'password' => md5($password),
            'nickname' => $nickname
        );
        $df = $this->m_user->insertData('users',$data);
        if($df>=1){
            redirect(base_url());
            $this->session->set_flashdata('pesan','Selamat Anda Berhasil Daftar');
        }else{
            $this->session->set_flashdata('pesan','Username dan password harus diisi');
            redirect('welcome');
        }
    }
	
    function proses(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username','Username','required|trim|xss_clean');
        $this->form_validation->set_rules('password','password','required|trim|xss_clean');
        
        if($this->form_validation->run()==false){
            $this->session->set_flashdata('pesan','Username dan password harus diisi');
            redirect('welcome');
        }else{
            $username=$this->input->post('username');
            $password=$this->input->post('password');
            $cek_user=$this->m_user->cek_user($username,md5($password));
            if($cek_user->num_rows()>0){
                //login berhasil, buat session
                    $this->session->set_userdata('username',$username);
                    redirect('user');                
            }
            else{
                //login gagal
                $this->session->set_flashdata('pesan','Username atau password salah');
                redirect('welcome');
            }
        }
    }
}