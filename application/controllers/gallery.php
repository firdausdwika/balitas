<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('m_gallery');
        $this->load->library(array('form_validation'));
        
        if(!$this->session->userdata('username')){
            redirect('welcome');
        }
    }

	function index()
	{
		$data = $this->m_gallery->getData();
		$this->load->view('gallery',array('data' => $data));
	}

	function add_new()
	{
		$this->load->view('add_gallery');
	}

	public function do_insert()
	{
		if($_FILES['featured']['name'] != "")
		{
		$config['upload_path']='uploads/gallery';
		$config['allowed_types']='gif|jpg|png|jpeg';
		$config['max_size']='1000000';
		$config['remove_spaces']=true;
		$config['overwrite']=false;
		$config['encrypt_name']=true;
		$config['max_width']='';
		$config['max_height']='';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('featured'))
		{
			redirect('gallery');
			exit();
		}
		else
		{
			$image = $this->upload->data();
			if($image['file_name'])
			{
				$data['file1'] = $image['file_name'];
			}
			$product_image = $data['file1'];
			$config['image_library'] = 'gd2';
        	$config['source_image'] = '/uploads/gallery/'.$data['file1'];
        	$config['new_image'] = '/uploads/new/';
        	$config['maintain_ratio'] = FALSE;
        	$config['overwrite'] = false;
        	$config['width'] = 1024;
        	$config['height'] = 768;
        	$this->load->library('image_lib', $config); //load library
        	$this->image_lib->clear();
        	$this->image_lib->initialize($config);
        	$this->image_lib->resize();
		}
		$file_data = $this->upload->data();
		$data = array(
            'foto' => $file_data['file_name']
        );
		$res = $this->m_gallery->insertData('gallery',$data);
		redirect('gallery');
		}
}

	public function edit($id)
	{
		$res = $this->m_gallery->editData(" where id_user = '$id'");
		$data = array(
			"id" => $res[0]['id_user'],
			"username" => $res[0]['username'],
			"password" => $res[0]['password'],
			"nickname" => $res[0]['nickname']
			);
		$this->load->view('edit_user',$data);
	}	

	public function do_update()
	{
		$id = $_POST['id_user'];
		$username = $_POST['username'];
		$nickname = $_POST['nickname'];
		$data = array(
			'username' => $username,
			'nickname' => $nickname 
		);
		$where = array('id_user' => $id);
		$res = $this->m_gallery->updateData('gallery',$data,$where);
		redirect('user');
	}

	public function do_delete($id)
	{
		$where = array('id_gallery' => $id);
		$res = $this->m_gallery->deleteData('gallery',$where);
		redirect('gallery');	
	}	

	function _set_rules(){
        $this->form_validation->set_rules('user','username','required|trim');
        $this->form_validation->set_rules('password','password','required|trim');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'>","</div>");
    }
    
    function logout(){
        $this->session->unset_userdata('username');
        redirect(base_url());
    }
}