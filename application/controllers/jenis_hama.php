<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenis_Hama extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('m_hama');
        $this->load->library(array('form_validation'));
        
        if(!$this->session->userdata('username')){
            redirect('welcome');
        }
    }

	function index()
	{
		$data = $this->m_hama->getData();
		$this->load->view('jenis_hama',array('data' => $data));
	}

	function add_new()
	{
		$this->load->view('add_hama');
	}

	public function do_insert()
	{
		if($_FILES['foto']['name'] != "")
		{
		$config['upload_path']='uploads/hama';
		$config['allowed_types']='gif|jpg|png|jpeg';
		$config['max_size']='1000000';
		$config['remove_spaces']=true;
		$config['overwrite']=false;
		$config['encrypt_name']=true;
		$config['max_width']='';
		$config['max_height']='';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('foto'))
		{
			redirect('jenis_hama');
			exit();
		}
		else
		{
			$image = $this->upload->data();
			if($image['file_name'])
			{
				$data['file1'] = $image['file_name'];
			}
			$product_image = $data['file1'];
			$config['image_library'] = 'gd2';
        	$config['source_image'] = '/uploads/'.$data['file1'];
        	$config['new_image'] = '/uploads/new/';
        	$config['maintain_ratio'] = FALSE;
        	$config['overwrite'] = false;
        	$config['width'] = 1024;
        	$config['height'] = 768;
        	$this->load->library('image_lib', $config); //load library
        	$this->image_lib->clear();
        	$this->image_lib->initialize($config);
        	$this->image_lib->resize();
		}
		$nama = $_POST['nama'];
		$latin = $_POST['latin'];
		$deskripsi = $_POST['deskripsi'];
		$file_data = $this->upload->data();
		$data = array(
            'nama_hama' => $nama,
            'nama_latin' => $latin,
            'foto' => $file_data['file_name'],
            'deskripsi' => $deskripsi
            );
		$res = $this->m_hama->insertData('jenis_hama',$data);
		redirect('jenis_hama');
		}
	}

	public function edit($id)
	{
		$res = $this->m_hama->editData(" where id_hama = '$id'");
		$data = array(
			"id" => $res[0]['id_hama'],
			"nama_hama" => $res[0]['nama_hama'],
			"nama_latin" => $res[0]['nama_latin'],
			"foto" => $res[0]['foto'],
			"deskripsi" => $res[0]['deskripsi']
			);
		$this->load->view('edit_hama',$data);
	}	

	public function do_update()
	{
		$id = $_POST['id_hama'];
		$username = $_POST['nama_hama'];
		$nickname = $_POST['nama_latin'];
		$nickname2 = $_POST['deskripsi'];
		$data = array(
			'nama_hama' => $username,
			'nama_latin' => $nickname,
			'deskripsi' => $nickname2 
		);
		$where = array('id_hama' => $id);
		$res = $this->m_hama->updateData('jenis_hama',$data,$where);
		redirect('jenis_hama');
	}

	public function do_delete($id)
	{
		$where = array('id_hama' => $id);
		$res = $this->m_hama->deleteData('jenis_hama',$where);
		redirect('jenis_hama');	
	}	

	function _set_rules(){
        $this->form_validation->set_rules('user','username','required|trim');
        $this->form_validation->set_rules('password','password','required|trim');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'>","</div>");
    }
    
    function logout(){
        $this->session->unset_userdata('username');
        redirect(base_url());
    }
}