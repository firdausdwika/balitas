<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Identifikasi extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('m_identifikasi');
        $this->load->library(array('form_validation'));
        
        if(!$this->session->userdata('username')){
            redirect('welcome');
        }
    }

	function index()
	{
		$data = $this->m_identifikasi->getData();
		$this->load->view('identifikasi',array('data' => $data));
	}

	function add_new()
	{
		$this->load->view('add_identifikasi');
	}

	public function do_insert()
	{
		$gejala = $_POST['gejala'];
        $perilaku = $_POST['perilaku'];
        $deskripsi = $_POST['deskripsi'];
        $data = array(
            'gejala_serangan' => $gejala,
            'perilaku' => $perilaku,
            'deskripsi' => $deskripsi
        );
		$res = $this->m_identifikasi->insertData('identifikasi',$data);
		redirect('identifikasi');
	}

	public function edit($id)
	{
		$res = $this->m_identifikasi->editData(" where id_identifikasi = '$id'");
		$data = array(
			"id" => $res[0]['id_identifikasi'],
			"gejala_serangan" => $res[0]['gejala_serangan'],
			"perilaku" => $res[0]['perilaku'],
			"deskripsi" => $res[0]['deskripsi']
			);
		$this->load->view('edit_identifikasi',$data);
	}	

	public function do_update()
	{
		$id = $_POST['id_identifikasi'];
		$username = $_POST['gejala_serangan'];
		$nickname = $_POST['deskripsi'];
		$data = array(
			'gejala_serangan' => $username,
			'deskripsi' => $nickname 
		);
		$where = array('id_identifikasi' => $id);
		$res = $this->m_identifikasi->updateData('identifikasi',$data,$where);
		redirect('identifikasi');
	}

	public function do_delete($id)
	{
		$where = array('id_identifikasi' => $id);
		$res = $this->m_identifikasi->deleteData('identifikasi',$where);
		redirect('identifikasi');	
	}		

	function _set_rules(){
        $this->form_validation->set_rules('user','username','required|trim');
        $this->form_validation->set_rules('password','password','required|trim');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'>","</div>");
    }
    
    function logout(){
        $this->session->unset_userdata('username');
        redirect(base_url());
    }
}