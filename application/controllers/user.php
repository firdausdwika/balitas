<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('m_user');
        $this->load->library(array('form_validation'));
        
        if(!$this->session->userdata('username')){
            redirect('user');
        }
    }

	function index()
	{
		$data = $this->m_user->getData();
		$this->load->view('home',array('data' => $data));
	}

	function add_new()
	{
		$this->load->view('add_user');
	}

	public function do_insert()
	{
		$username = $_POST['username'];
        $password = $_POST['password'];
        $nickname = $_POST['nickname'];
        $data = array(
            'username' => $username,
            'password' => md5($password),
            'nickname' => $nickname
        );
		$res = $this->m_user->insertData('users',$data);
		redirect('user');
	}

	public function edit($id)
	{
		$res = $this->m_user->editData(" where id_user = '$id'");
		$data = array(
			"id" => $res[0]['id_user'],
			"username" => $res[0]['username'],
			"password" => $res[0]['password'],
			"nickname" => $res[0]['nickname']
			);
		$this->load->view('edit_user',$data);
	}	

	public function do_update()
	{
		$id = $_POST['id_user'];
		$username = $_POST['username'];
		$nickname = $_POST['nickname'];
		$data = array(
			'username' => $username,
			'nickname' => $nickname 
		);
		$where = array('id_user' => $id);
		$res = $this->m_user->updateData('users',$data,$where);
		redirect('user');
	}

	public function do_delete($id)
	{
		$where = array('id_user' => $id);
		$res = $this->m_user->deleteData('users',$where);
		redirect('user');	
	}	

	function _set_rules(){
        $this->form_validation->set_rules('user','username','required|trim');
        $this->form_validation->set_rules('password','password','required|trim');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'>","</div>");
    }
    
    function logout(){
        $this->session->unset_userdata('username');
        redirect(base_url());
    }
}