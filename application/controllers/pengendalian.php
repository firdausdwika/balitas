<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengendalian extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('m_pengendalian');
        $this->load->library(array('form_validation'));
        
        if(!$this->session->userdata('username')){
            redirect('welcome');
        }
    }

	function index()
	{
		$data = $this->m_pengendalian->getData();
		$this->load->view('pengendalian',array('data' => $data));
	}

	function add_new()
	{
		$this->load->view('add_pengendalian');
	}

	public function do_insert()
	{
		if($_FILES['gambar']['name'] != "")
		{
		$config['upload_path']='uploads/pengendalian';
		$config['allowed_types']='gif|jpg|png|jpeg';
		$config['max_size']='1000000';
		$config['remove_spaces']=true;
		$config['overwrite']=false;
		$config['encrypt_name']=true;
		$config['max_width']='';
		$config['max_height']='';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('gambar'))
		{
			redirect('pengendalian');
			exit();
		}
		else
		{
			$image = $this->upload->data();
			if($image['file_name'])
			{
				$data['file1'] = $image['file_name'];
			}
			$product_image = $data['file1'];
			$config['image_library'] = 'gd2';
        	$config['source_image'] = '/uploads/'.$data['file1'];
        	$config['new_image'] = '/uploads/new/';
        	$config['maintain_ratio'] = FALSE;
        	$config['overwrite'] = false;
        	$config['width'] = 1024;
        	$config['height'] = 768;
        	$this->load->library('image_lib', $config); //load library
        	$this->image_lib->clear();
        	$this->image_lib->initialize($config);
        	$this->image_lib->resize();
		}
		$keterangan = $_POST['keterangan'];
		$file_data = $this->upload->data();
		$data = array(
            'gambar' => $file_data['file_name'],
            'deskripsi' => $keterangan
            );
		$res = $this->m_pengendalian->insertData('pengendalian',$data);
		redirect('pengendalian');
		}
	}

	public function edit($id)
	{
		$res = $this->m_pengendalian->editData(" where id_pengendalian = '$id'");
		$data = array(
			"id" => $res[0]['id_pengendalian'],
			"gambar" => $res[0]['gambar'],
			"deskripsi" => $res[0]['deskripsi']
			);
		$this->load->view('edit_pengendalian',$data);
	}	

	public function do_update()
	{
		$id = $_POST['id_pengendalian'];
		$nickname = $_POST['deskripsi'];
		$data = array(
			'deskripsi' => $nickname 
		);
		$where = array('id_pengendalian' => $id);
		$res = $this->m_pengendalian->updateData('pengendalian',$data,$where);
		redirect('pengendalian');
	}

	public function do_delete($id)
	{
		$where = array('id_pengendalian' => $id);
		$res = $this->m_pengendalian->deleteData('pengendalian',$where);
		redirect('pengendalian');	
	}	

	function _set_rules(){
        $this->form_validation->set_rules('user','username','required|trim');
        $this->form_validation->set_rules('password','password','required|trim');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'>","</div>");
    }
    
    function logout(){
        $this->session->unset_userdata('username');
        redirect(base_url());
    }
}