<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Siklus_Hidup extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('m_siklus');
        $this->load->library(array('form_validation'));
        
        if(!$this->session->userdata('username')){
            redirect('welcome');
        }
    }

	function index()
	{
		$data = $this->m_siklus->getData();
		$this->load->view('siklus_hidup',array('data' => $data));
	}

	function add_new()
	{
		$this->load->view('add_siklus');
	}

	public function do_insert()
	{
		if($_FILES['siklus']['name'] != "")
		{
		$config['upload_path']='uploads/siklus_hidup';
		$config['allowed_types']='gif|jpg|png|jpeg';
		$config['max_size']='1000000';
		$config['remove_spaces']=true;
		$config['overwrite']=false;
		$config['encrypt_name']=true;
		$config['max_width']='';
		$config['max_height']='';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('siklus'))
		{
			redirect('siklus_hidup');
			exit();
		}
		else
		{
			$image = $this->upload->data();
			if($image['file_name'])
			{
				$data['file1'] = $image['file_name'];
			}
			$product_image = $data['file1'];
			$config['image_library'] = 'gd2';
        	$config['source_image'] = '/uploads/'.$data['file1'];
        	$config['new_image'] = '/uploads/new/';
        	$config['maintain_ratio'] = FALSE;
        	$config['overwrite'] = false;
        	$config['width'] = 1024;
        	$config['height'] = 768;
        	$this->load->library('image_lib', $config); //load library
        	$this->image_lib->clear();
        	$this->image_lib->initialize($config);
        	$this->image_lib->resize();
		}
		$keterangan = $_POST['keterangan'];
		$file_data = $this->upload->data();
		$data = array(
            'siklus_hidup' => $file_data['file_name'],
            'keterangan' => $keterangan
            );
		$res = $this->m_siklus->insertData('siklus_hidup',$data);
		redirect('siklus_hidup');
		}
	}

	public function edit($id)
	{
		$res = $this->m_siklus->editData(" where id_siklus = '$id'");
		$data = array(
			"id" => $res[0]['id_siklus'],
			"siklus_hidup" => $res[0]['siklus_hidup'],
			"keterangan" => $res[0]['keterangan']
			);
		$this->load->view('edit_siklus',$data);
	}	

	public function do_update()
	{
		$id = $_POST['id_siklus'];
		$nickname = $_POST['keterangan'];
		$data = array(
			'keterangan' => $nickname 
		);
		$where = array('id_siklus' => $id);
		$res = $this->m_siklus->updateData('siklus_hidup',$data,$where);
		redirect('siklus_hidup');
	}

	public function do_delete($id)
	{
		$where = array('id_siklus' => $id);
		$res = $this->m_siklus->deleteData('siklus_hidup',$where);
		redirect('siklus_hidup');	
	}	
}