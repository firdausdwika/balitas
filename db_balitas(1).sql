-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2015 at 06:34 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_balitas`
--

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id_gallery` int(11) NOT NULL AUTO_INCREMENT,
  `foto` varchar(100) NOT NULL,
  PRIMARY KEY (`id_gallery`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id_gallery`, `foto`) VALUES
(1, 'gs_grayak.png'),
(2, 'gs_kdaun.jpg'),
(3, 'gs_pupus.jpg'),
(4, 'gs_apisdanmisus.jpg'),
(5, 'gs_virus.png'),
(6, 'gs_pupus2.png'),
(7, 'gs_trips.png');

-- --------------------------------------------------------

--
-- Table structure for table `identifikasi`
--

CREATE TABLE IF NOT EXISTS `identifikasi` (
  `id_identifikasi` int(11) NOT NULL AUTO_INCREMENT,
  `gejala_serangan` varchar(100) NOT NULL,
  `perilaku` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_identifikasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `identifikasi`
--

INSERT INTO `identifikasi` (`id_identifikasi`, `gejala_serangan`, `perilaku`, `deskripsi`) VALUES
(1, 'gejala', 'penyakit_kerupuk.jpg', 'gejala2');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_hama`
--

CREATE TABLE IF NOT EXISTS `jenis_hama` (
  `id_hama` int(11) NOT NULL AUTO_INCREMENT,
  `nama_hama` varchar(100) NOT NULL,
  `nama_latin` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_hama`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `jenis_hama`
--

INSERT INTO `jenis_hama` (`id_hama`, `nama_hama`, `nama_latin`, `foto`, `deskripsi`) VALUES
(1, 'Ulat Pupus2', 'nama_latin', 'pupus.png', 'Ulat pupus pada tanaman tembakau terdiri dari dua jenis yaitu Helicoverpa assulta Gn. dan H. armigera (Lepidoptera: Noctuidae). Sesuai dengan namanya, hama ini biasanya memakan pupus atau daun-daun yang masih muda. Lubang bekas serangan pada awalnya terlihat kecil dan akan menjadi jelas serta besar seiring dengan perkembangan dan pertumbuhan daun. Serangan hama ini dapat menyebabkan kerusakan yang serius pada daun. Selain pada daun, serangga ini juga menggerak buah.'),
(2, 'Ulat Grayak', 'nama_latin', 'grayak.jpg', 'Ulat grayak mempunyai nama ilmiah Spodoptera litura Fabricus (Lepidoptera; Noctuidae). Hama ini umum ditemukan pada daun tanaman tembakau. Larva instar 1-2 berkelompok makan secara bersama di bawah permukaan daun dan menyisakan lapisan epidermis atas sehingga daun terlihat transparan (Gambar 1). Pada instar yang lebih lanjut, ulat makan seluruh daun sehingga menyebabkan daun berlubang-lubang. Pada serangan yang parah dapat menghabiskan seluruh daun tanaman.'),
(3, 'Ulat Tanah', 'nama_latin', 'ulat_tanah.png', 'Hama ini dikenal juga sebagai Agrotis ipsilon Huf. (Lepidoptera; Noctuidae). Larva instar awal menyebabkan daun-daun tanaman di pembibitan berlubang-lubang, sedangkan instar 3 sampai 7 menyerang tanaman dengan cara memotong pangkal batang tanaman tembakau sehingga menyebabkan tanaman rebah dan mati. Mereka menyerang pada malam hari sedangkan pada siang hari bersembunyi di dalam tanah berdasar perilaku inilah nama ulat tanah diberikan'),
(4, 'Ulat Jengkal', 'nama_latin', 'jengkal.png', 'Ulat jenis ini memiliki beberapa jenis :P. signata;\r\nP. chacites;\r\nP. orichalcea.\r\nWarna ulat hijau daun, tidak mempunyai tungkai palsu, sehingga jika berjalan badannya melengkung ke atas seperti “kilan”. Lebih menyukai daun-daun yang sudah tua. \r\n'),
(5, 'Kutu Daun', 'nama_latin', 'k_daun.png', 'Terdapat dua jenis kutu yang umum ditemukan pada tanaman tembakau yaitu Myzus persicae Sulz. dan Aphis gossypii yang merupakan anggota ordo Hemiptera dan famili  Aphididae. Keduanya mengisap cairan tanaman sehingga menyebabkan bercak-bercak pada daun. Kutu ini memiliki arti yang cukup penting karena berperan sebagai vektor atau penular beberapa jenis virus di antaranya penyebab Cucumber Mozaic Virus (CMV) pada tembakau.'),
(6, 'Kutu Kebul', 'nama_latin', 'k_kebul.jpg', 'Kutu ini dikenal dengan nama Bemisia tabaci Genn. yang termasuk dalam ordo Hemiptera dan famili Aleyrodidae. Pemberian nama kutu kebul ini terkait dengan perilaku kutu  yang akan beterbangan seperti kebul atau asap putih ketika merasa terganggu.'),
(7, 'Ulat Penggerek Batang', 'nama_latin', 'u_penggerek.jpg', 'Warna ulat putih kotor dengan kepala hitam dan dilengkapi perisai sebagai pelindung.  Telur diletakkan secara tunggal pada pangkal batang. Panjang ulat dewasa 11 mm.  Pupanya berada dalam lubang gerekan pada batang. \r\n'),
(8, 'Trips', 'nama_latin', 'trips2.png', 'Tanaman. inang : b. merah, b. putih, cabai, kentang, terung, waluh, tembakau, mentimun, semangka dan kacang-kacangan.  \nPartenogenesis, siklus hidup: telur, nimfa, pupa dan imago, daur hdup 11-17 hari.\nSeekor serangga betina mampu menghasilkan telur : 80 butir. Serangga dewasa berwarna kuning coklat kehitaman, berukuran panjang 0,8-0,9 mm \nMusuh alami : kumbang Coccinellidae, tungau predator, dan larva Chrysopidae.'),
(9, 'Capsid Tembakau', 'nama_latin', 'capsid.png', 'Hama ini dikenal dengan nama Nesidiocoris (=Cyrtopeltis) tenuis Reuter (Hemiptera; Miridae). Serangga ini menghisap cairan sel daun tanaman tembakau sehingga menimbulkan nekrosis pada daun sehingga mudah robek. Selain menjadi hama, serangga ini juga mengisap telur dan larva ulat jengkal dan ulat grayak, kutu daun, atau serangga-serangga yang telah mati.  '),
(10, 'Anjing Tanah/Orong-orong', 'nama_latin', 'orong2.png', 'Anjing tanah = Gryllotalpa africana Pal. (Orthoptera; Gryllotalpidae).\r\n\r\nSayap pada betinanya berkembang separuh bagian sedangkan jantannya dapat mengerik pada saat senja hari.\r\n \r\nTelur berukuran panjang 2,5 mm, diletakkan dalam lubang tanah yang dalam.  Memakan humus serta akar-akar muda sehingga tanaman mejadi kuning dan layu.\r\n\r\nPanjang tubuhnya berkisar 26-36 mm. \r\n\r\nHama ini bersifat  polyphagous\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `pengendalian`
--

CREATE TABLE IF NOT EXISTS `pengendalian` (
  `id_pengendalian` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_pengendalian`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pengendalian`
--

INSERT INTO `pengendalian` (`id_pengendalian`, `gambar`, `deskripsi`) VALUES
(1, 'penyakit_kerupuk.jpg', 'dasdasdsadasd2');

-- --------------------------------------------------------

--
-- Table structure for table `siklus_hidup`
--

CREATE TABLE IF NOT EXISTS `siklus_hidup` (
  `id_siklus` int(11) NOT NULL AUTO_INCREMENT,
  `siklus_hidup` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_siklus`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `siklus_hidup`
--

INSERT INTO `siklus_hidup` (`id_siklus`, `siklus_hidup`, `keterangan`) VALUES
(1, 'sh_pupus.png', 'Telur(3-8 hari)Larva(13-21 hari)Pupa(8-15 hari)Imago(11-14hari)2'),
(2, 'sh_grayak.png', 'Telur(2-3 hari)\r\nLarva(20-46 hari)\r\nPupa(7-10 hari)\r\nImago(6-8 hari)');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nickname` varchar(100) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`, `nickname`) VALUES
(1, 'suad', '8253c06bad235d7a7418a24ececd9b96', 'Firdaus');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
